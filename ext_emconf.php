<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "setdefaultauthor".
 *
 * Auto generated 14-04-2016 16:46
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Set default author/email',
  'description' => 'Defaults the author/email fields in pages to the info from the currently logged in user',
  'category' => 'be',
  'version' => '0.0.3',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearcacheonload' => true,
  'author' => 'Mike Tölle',
  'author_email' => 'kontakt@typo3graf.de',
  'author_company' => 'Typo3graf media-agentur',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '6.2.0-7.9.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

