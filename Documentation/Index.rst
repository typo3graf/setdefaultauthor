﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _start:

=============================================================
Set default author/email
=============================================================

.. only:: html

	:Classification:
		setdefaultauthor

	:Version:
		0.0.3

	:Language:
		en

	:Description:
		Defaults the author/email fields in pages to the info from the currently logged in user.

	:Keywords:
		default,author

	:Copyright:
		2015

	:Author:
		Mike Tölle

	:Email:
		mike.toelle@typo3graf.de

	:License:
		This document is published under the Open Publication License
		available from http://www.opencontent.org/openpub/

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <https://typo3.org/>`__.


	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:



	AdministratorManual/Index

