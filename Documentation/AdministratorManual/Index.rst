﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _admin-manual:

Administrator Manual
====================

This chapter describes how to manage the extension.


Installation
------------

The extension needs to be installed as any other extension of TYPO3 CMS:

Switch to the module “Extension Manager”.

Get it from the Extension Manager: Press the “Retrieve/Update” button and search for the extension key **setdefaultauthor** and import the extension from the repository.

**Nothing else to do.**





