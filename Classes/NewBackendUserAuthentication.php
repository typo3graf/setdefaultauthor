<?php
namespace T3g\Setdefaultauthor;
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Mike Tölle <mike.toelle@typo3graf.de>, Typo3graf media-agentur
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class NewBackendUserAuthentication extends \TYPO3\CMS\Core\Authentication\BackendUserAuthentication {
   function fetchGroupData()	{
	   if ($this->user['uid'])	{
		   
		  /* if (\TYPO3\CMS\Core\Package\PackageManager::isLoaded('sys_note')) {
				$this->TSdataArray[]='
					// Setting defaults for sys_note author / email...
					TCAdefaults.sys_note.author = '.$this->user['realName'].'
					TCAdefaults.sys_note.email = '.$this->user['email'].'
				';
			}*/
			
		   $this->TSdataArray[]='
				// Setting defaults for pages author / email...
				TCAdefaults.pages.author = '.$this->user['realName'].'
				TCAdefaults.pages.author_email = '.$this->user['email'].'
					
			';
	   }
	   parent::fetchGroupData();
   }	
}