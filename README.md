#**Set default Author/Email Extension for TYPO3**

##What does it do?

Defaults the author/email fields in pages to the info from the currently logged in user.